/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.*;

/**
 *
 * @author root
 */
public class Time {
    
    private HashMap<String, Jogador> jogadores;
   
    public List<Jogador> ordena(JogadorComparator jc){
                   
        List<Jogador> list = new ArrayList<>(jogadores.size());
         
        Jogador [] l = new Jogador[jogadores.size()];
        
        int i=0;                        
        Set<Map.Entry<String, Jogador>> t1 = jogadores.entrySet();
        for (Map.Entry<String, Jogador> entry1 : t1) {
            //list.add(i++,entry1.getValue()); //Adicionou um jogador ah lista
            l[i++]=entry1.getValue();
        }
                
        for(i=0;i<l.length-1;i++){ //1 elemento, o ultimo jah estah ordenado
            System.out.println("\n"+l[i]+"\n");
            for(int j=0;j<l.length;j++){ //todos os outros
                System.out.println("["+l[i] + "] ["+l[j]+"]");
                if(jc.compare(l[i], l[j])>0){                    
                    Jogador aux1 = l[i];                                        
                    l[i]=l[j];
                    l[j]=aux1;
                }                
            }
        }
        /*//Set<String> s = jogadores.keySet();
        for(Map.Entry<String, Jogador> entry1:jogadores.entrySet()){
            for(Map.Entry<String, Jogador> entry2:jogadores.entrySet()){
                if (j.compare((Jogador)entry1, (Jogador)entry2)>0)
                    troca((Jogador)entry1,(Jogador)entry2);
            }
        }
        */  
        i=0;
        while(i<l.length){
            list.add(i,l[i++]);
        }
        
        return list;
    }
     
    public void troca(){
        
    }
    
    public Time(){
        this.jogadores = new HashMap<>();
    }
    
    public HashMap<String, Jogador> getJogadores(){
      return this.jogadores;  
    }
    
    public void addJogador(String s, Jogador j){
        this.jogadores.put(s,j);
    }
}
