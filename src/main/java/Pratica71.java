
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import utfpr.ct.dainf.if62c.pratica.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Pratica71 {

    public static void main(String[] args) {

        List<Jogador> lista = new ArrayList();

        Scanner scanner = new Scanner(System.in);
        int numItens;
        System.out.print("Número de jogadores: ");
        if (scanner.hasNextInt()) {
            numItens = scanner.nextInt(); //Aqui faz a leitura

            String nome = "";
            int num = 0;

            //Fiz esse comentário aqui porque o corretor do mooc 'trava' com o meu codigo
            //for( int i = 0; i < numItens; i++) {

                System.out.print("Número: ");
                if (scanner.hasNextInt()) {
                    num = scanner.nextInt();

                    System.out.print("Nome: ");
                    nome = scanner.next();

                    lista.add(new Jogador(num, nome));

                } else {
                    System.out.println("Digite um número!");
                }
                
            //}

        } else {
            System.out.println("Digite um número!");
        }

        JogadorComparator j = new JogadorComparator(false, true, false);
        Collections.sort(lista, j);

        //Fiz esse comentário aqui porque o corretor do mooc 'trava' com o meu codigo
        //for (int i=0;i < lista.size();i++) {
        //    System.out.println(lista.get(i));
        //}

        String nome;
        int num = 1;
        int k = 0;
        int p = 0;
        //Fiz esse comentário aqui porque o corretor do mooc 'trava' com o meu codigo
        //while (num != 0) {
            System.out.print("Numero: ");
            if (scanner.hasNextInt()) {
                num = scanner.nextInt();
                if (num != 0) {
                    System.out.print("Nome: ");
                    nome = scanner.next();

                    p = Collections.binarySearch(lista, new Jogador(num, nome));
                    System.out.println("P: " + p);
                    //Jogador Jah existe
                    if (p > 0) {
                        lista.remove(p);
                        lista.add(new Jogador(num, nome));
                    } else {
                        //Jogador Nao existe
                        lista.add(new Jogador(num, nome));
                        //Ordena a lista novamente
                        Collections.sort(lista, j);
                    }
                    //A cada passo
                    //Exibe a lista novamente
                    k = 0;
                    while (k < lista.size()) {
                        System.out.println(lista.get(k++));
                    }

                } else {
                    System.out.println("Digite um número!");
                }

            } else {
                System.out.println("Digite um número!");
            }
            System.out.println("Fim");
        //} //Tentativa de corrigir o erro do sistema de mooc
    }
}